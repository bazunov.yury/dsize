package dsize

import (
	"fmt"
	"math"
)

const (
	_ = 1 << (10 * iota)
	KiloByte
	MegaByte
	GigaByte
	TeraByte
	PetaByte
	ExaByte
)

var Units = map[uint64]string{
	KiloByte: "Kb",
	MegaByte: "Mb",
	GigaByte: "Gb",
	TeraByte: "Tb",
	PetaByte: "Pb",
	ExaByte:  "Eb",
}

type Size int64

func (d Size) String() string {
	unit := ""
	value := float64(d)

	switch {
	case d >= ExaByte:
		unit = Units[ExaByte]
		value = value / ExaByte
	case d >= PetaByte:
		unit = Units[PetaByte]
		value = value / PetaByte
	case d >= TeraByte:
		unit = Units[TeraByte]
		value = value / TeraByte
	case d >= GigaByte:
		unit = Units[GigaByte]
		value = value / GigaByte
	case d >= MegaByte:
		unit = Units[MegaByte]
		value = value / MegaByte
	case d >= KiloByte:
		unit = Units[KiloByte]
		value = value / KiloByte
	case d >= 0:
		unit = "B"
	}

	return fmt.Sprintf("%v %s", math.Floor(value*100)/100, unit)
}
