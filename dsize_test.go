package dsize

import (
	"testing"
)

func TestSize_String(t *testing.T) {
	table := [...]struct {
		input    Size
		expected string
	}{
		{0, "0 B"},
		{1, "1 B"},
		{5, "5 B"},
		{8, "8 B"},
		{9, "9 B"},
		{1023, "1023 B"},
		{1024, "1 " + Units[KiloByte]},
		{1025, "1 " + Units[KiloByte]},
		{2050, "2 " + Units[KiloByte]},
		{5000, "4.88 " + Units[KiloByte]},
		{1024 * 1024, "1 " + Units[MegaByte]},
		{2689792, "2.56 " + Units[MegaByte]},
		{1024 * 1024 * 1024, "1 " + Units[GigaByte]},
		{1024 * 1024 * 1024 * 1024, "1 " + Units[TeraByte]},
		{1024 * 1024 * 1024 * 1024 * 1024, "1 " + Units[PetaByte]},
		{1024 * 1024 * 1024 * 1024 * 1024 * 1024, "1 " + Units[ExaByte]},
	}
	for i, row := range table {
		if row.input.String() != row.expected {
			t.Errorf("%d: dsize.Size(%d).String() = %v, expected %v", i+1, row.input, row.input, row.expected)
		}
	}
}
